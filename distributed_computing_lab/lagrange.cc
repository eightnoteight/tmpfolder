#include <bits/stdc++.h>
#include "mpi.h"

using namespace std;



double interpolated_function(double x) {
	return x * x;
}


double lagrange_interpolate_ith_term(double x, double* array, double length, int i) {
	double numer = 1;
	for (int j = 0; j < length; ++j) {
		if (i != j) {
			numer *= x - j;
		}
	}
	double denom = 1;
	for (int j = 0; j < length; ++j) {
		if (i != j) {
			denom *= i - j;
		}
	}
	return (numer / denom) * interpolated_function(i);
}


int main(int argc, char** argv) {
	int my_rank;
	int p, src, dest, tag = 0;
	char message[100];
	MPI_Status status;
	MPI_Init(&argc, &argv);
	MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);
	MPI_Comm_size(MPI_COMM_WORLD, &p);
	double* array = new double[p];
	if (my_rank == 0) {
		scanf("%lf", &array[0]);
		for (int i = 1; i < p; ++i) {
			array[i] = interpolated_function(i);
		}
	}
	MPI_Bcast(array, p, MPI_DOUBLE, 0, MPI_COMM_WORLD);
	double local_result[] = {0};
	if (my_rank != 0) {
		local_result[0] = lagrange_interpolate_ith_term(array[0], array + 1, p - 1, my_rank - 1);
	}
	double global_sum[] = {0};
	MPI_Reduce(local_result, global_sum, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);
	if (my_rank == 0) {
		printf("interpolated_function( %lf ) = %lf\n", array[0], global_sum[0]);
	}
	MPI_Finalize();
	return 0;
}

