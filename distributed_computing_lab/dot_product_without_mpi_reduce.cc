#include <bits/stdc++.h>
#include <stdio.h>
#include <string.h>
#include "mpi.h"

using namespace std;

int main(int argc, char** argv) {
	int my_rank;
	int p, src, dest, tag = 0;
	char message[100];
	MPI_Status status;
	MPI_Init(&argc, &argv);
	MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);
	MPI_Comm_size(MPI_COMM_WORLD, &p);
	int vec1[] = {1, 2, 3, 4, 5, 6, 7};
	int vec2[] = {2, 3, 4, 5, 6, 7, 8};
	int i;
	int v1, v2;
	if (my_rank != 0) {
		sprintf(message, "%d %d\n", vec1[my_rank - 1], vec2[my_rank - 1]);
		dest = 0;
		MPI_Send(message, strlen(message) + 1, MPI_CHAR, dest, tag, MPI_COMM_WORLD);
	} else {
		int dotprod = 0;
		for (src = 1; src < p; ++src) {
			MPI_Recv(message, 100, MPI_CHAR, src, tag, MPI_COMM_WORLD, &status);
			sscanf(message, "%d %d", &v1, &v2);
			dotprod += v1*v2;
		}
		printf("%d\n", dotprod);
		
	}
	MPI_Finalize();
	return 0;
}
