#include <bits/stdc++.h>
#include <stdio.h>
#include <string.h>
#include "mpi.h"

using namespace std;

int main(int argc, char** argv) {
	int my_rank;
	int p, src, dest, tag = 0;
	char message[100];
	MPI_Status status;
	MPI_Init(&argc, &argv);
	MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);
	MPI_Comm_size(MPI_COMM_WORLD, &p);
	int vec1[] = {1, 2, 3, 4, 5, 6, 7};
	int vec2[] = {2, 3, 4, 5, 6, 7, 8};
	int i;
	int v1, v2;
	int lsum[] = {0};
	int gsum[] = {0};
	lsum[0] += vec1[my_rank] * vec2[my_rank];
	MPI_Reduce(lsum, gsum, 1, MPI_INT, MPI_SUM, 0, MPI_COMM_WORLD);
	if (my_rank == 0) {
		printf("%d\n", gsum[0]);
	}
	MPI_Finalize();
	return 0;
}
